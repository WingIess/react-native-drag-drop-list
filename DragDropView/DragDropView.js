import React, { useState, useEffect } from "react";
import {
  View,
  TouchableWithoutFeedback,
  Animated,
  PanResponder,
  Easing,
} from "react-native";

// HELPER FUNCTIONS
const calcPosition = (index, numPerRow, itemHeight, itemWidth, gap) => {
  const y = Math.floor(index / numPerRow) * (itemHeight + gap);
  const itemIndexInRow = index % numPerRow;
  const x = itemIndexInRow * (itemWidth + gap);
  return { x, y };
};

const calcContainerHeight = (dataLength, numPerRow, itemHeight, gap) =>
  Math.floor(dataLength / numPerRow) * (itemHeight + gap) +
  (dataLength % numPerRow ? itemHeight : 0);
const calcContainerWidth = (numPerRow, itemWidth, gap) =>
  (itemWidth + gap) * numPerRow - gap;

// FIXME: animation not working on current selected slanping to place
const DragDrop = ({
  itemWidth,
  itemHeight,
  gap,
  Item,
  data,
  numPerRow,
  onDragEnd,
  onTap,
}) => {
  // functions for inicialization
  const getNewPosition = (it, index) =>
    calcPosition(index, numPerRow, itemHeight, itemWidth, gap);
  const getItemCenter = () => ({ x: itemWidth / 2, y: itemHeight / 2 });
  const generateAnimations = () => {
    let initAnimations = {};
    data.forEach((it, index) => {
      initAnimations = {
        ...initAnimations,
        [it.id]: new Animated.ValueXY(gridItemPositions[index]),
      };
    });
    return initAnimations;
  };

  // DATA
  const [localData, setLocalData] = useState([...data]);
  const [originLocalData, setOriginLocalData] = useState(null);

  // POSITIONS
  const [gridItemPositions, setGridItemPositions] = useState(
    data.map(getNewPosition)
  );
  const [animations, setAnimations] = useState(generateAnimations);

  // SELECTED POSITIONS
  const [panCapture, setPanCapture] = useState(false);
  const [selectedOrigin, setSelectedOrigin] = useState(null);
  const [selectedX, setSelectedX] = useState(0);
  const [selectedY, setSelectedY] = useState(0);

  // VIEW PROPERTIES
  const [containerHeight, setContainerHeight] = useState(
    calcContainerHeight(data.length, numPerRow, itemHeight, gap)
  );
  const [containerWidth, setContainerWidth] = useState(
    calcContainerWidth(numPerRow, itemWidth, gap)
  );
  const [itemCenter, setItemCenter] = useState(getItemCenter);

  // ITEM SELECTORS
  const [tempSelected, setTempSelected] = useState(null);
  const [selectedItemId, setSelectedItemId] = useState(null);

  // BOOLEANS
  const [shouldAnimate, setShouldAniamte] = useState(true);
  const [
    animateSelectedBackToOrigin,
    setAnimateSelectedBackToOrigin,
  ] = useState(false);

  // SUBSCRIPTIONS
  useEffect(() => {
    setContainerHeight(
      calcContainerHeight(data.length, numPerRow, itemHeight, gap)
    );
    setContainerWidth(calcContainerWidth(numPerRow, itemWidth, gap));
    setGridItemPositions(data.map(getNewPosition));
  }, [data.length, itemWidth, itemHeight, numPerRow, gap]);

  useEffect(() => {
    setAnimations(generateAnimations);
  }, [data, gridItemPositions]);

  useEffect(() => {
    animateShifting(data, 0, 0);
    setLocalData(data);
  }, [animations]);

  useEffect(() => {
    setItemCenter(getItemCenter);
  }, [itemWidth, itemHeight]);

  // ANIMATIONS
  const animateShifting = (array, startAnimatingFrom, duration) => {
    for (let index = startAnimatingFrom; index < array.length; index++) {
      const it = array[index];
      const currentSelectedAniamtion = animations[it.id];
      if (selectedItemId === it.id) {
        Animated.sequence([
          Animated.timing(currentSelectedAniamtion, {
            toValue: { x: selectedX, y: selectedY },
            duration: 0,
            easing: Easing.linear,
            useNativeDriver: false,
          }),
          Animated.timing(currentSelectedAniamtion, {
            toValue: gridItemPositions[index],
            duration,
            easing: Easing.linear,
            useNativeDriver: false,
          }),
        ]).start(() => setShouldAniamte(true));
      } else {
        Animated.timing(currentSelectedAniamtion, {
          toValue: gridItemPositions[index],
          duration,
          easing: Easing.linear,
          useNativeDriver: false,
        }).start(() => setShouldAniamte(true));
      }
    }
  };

  const animateItemBackToOriginPosition = () => {
    const prevAnimation = animations[selectedItemId];
    Animated.sequence([
      Animated.timing(prevAnimation, {
        toValue: { x: selectedX, y: selectedY },
        duration: 0,
        useNativeDriver: false,
      }),
      Animated.spring(prevAnimation, {
        toValue: { x: selectedOrigin.x, y: selectedOrigin.y },
        friction: 5,
      }),
    ]).start(() => {
      setAnimateSelectedBackToOrigin(false);
      setShouldAniamte(true);
    });
  };

  // PANRESPONDER
  const onMove = (_, gestureState) => {
    if (selectedItemId) {
      let newPosition = {};
      setSelectedX((prevSelectedX) => {
        const newX = prevSelectedX + gestureState.dx;
        newPosition = { x: newX };
        return newX;
      });
      setSelectedY((prevSelectedY) => {
        const newY = prevSelectedY + gestureState.dy;
        newPosition = { ...newPosition, y: newY };
        if (shouldAnimate) {
          let foundIndex = gridItemPositions.findIndex((position) =>
            centerOfBoxIsInHitbox(position, newPosition)
          );
          const indexOfSelected = localData.findIndex(
            (it) => it.id === selectedItemId
          );
          if (foundIndex !== -1) {
            const filteredData = localData.filter(
              (it, index) => index !== indexOfSelected
            );
            const newCurrentData = [
              ...filteredData.slice(0, foundIndex),
              localData[indexOfSelected],
              ...filteredData.slice(foundIndex),
            ];
            const startingIndex =
              indexOfSelected > foundIndex ? foundIndex : indexOfSelected;
            setLocalData(newCurrentData);
            animateShifting(newCurrentData, startingIndex, 120);
            setAnimateSelectedBackToOrigin(false);
            setShouldAniamte(false);
          } else {
            const startingIndex = localData.findIndex(
              (it, index) => it.id !== originLocalData[index].id
            );
            setAnimateSelectedBackToOrigin(true);
            setLocalData(originLocalData);
            animateShifting(
              originLocalData,
              startingIndex === -1 ? localData.length : startingIndex,
              120
            );
            setShouldAniamte(startingIndex === -1);
          }
        }
        return newY;
      });
    }
  };

  const onRelease = () => {
    if (animateSelectedBackToOrigin) {
      animateItemBackToOriginPosition();
      setShouldAniamte(false);
    }
    setTempSelected(null);
    setPanCapture(false);
    setSelectedItemId(null);
    if (onDragEnd) {
      onDragEnd(localData);
    }
  };

  const onSelect = () => {
    if (panCapture) {
      const newCurrentSelectedIndex = localData.findIndex(
        (it) => tempSelected === it.id
      );
      selectItem(newCurrentSelectedIndex);
      setOriginLocalData(localData);
    }
  };

  const centerOfBoxIsInHitbox = (currentPostion, newPosition) =>
    currentPostion.x < newPosition.x + itemCenter.x &&
    currentPostion.x + itemWidth > newPosition.x + itemCenter.x &&
    currentPostion.y + itemHeight > newPosition.y + itemCenter.y &&
    currentPostion.y < newPosition.y + itemCenter.y;

  const selectItem = (index) => {
    const item = localData[index];
    const itemPosition = gridItemPositions[index];
    setPanCapture(true);
    setSelectedItemId(item.id);
    setSelectedOrigin(itemPosition);
    setSelectedX(itemPosition.x);
    setSelectedY(itemPosition.y);
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onStartShouldSetPanResponderCapture: () => false,
    onMoveShouldSetPanResponder: () => panCapture,
    onMoveShouldSetPanResponderCapture: () => panCapture,
    onPanResponderTerminationRequest: () => false,
    onShouldBlockNativeResponder: () => false,
    onPanResponderMove: onMove,
    onPanResponderGrant: onSelect,
    onPanResponderRelease: onRelease,
  });

  return (
    <View
      {...panResponder.panHandlers}
      style={{
        width: containerWidth,
        height: containerHeight,
        backgroundColor: "red",
      }}
    >
      {localData.map((it, index) => (
        <Animated.View
          key={it.id}
          style={[
            {
              position: "absolute",
              height: itemHeight,
              width: itemWidth,
            },
            selectedItemId === it.id
              ? {
                  transform: [
                    { translateX: selectedX },
                    { translateY: selectedY },
                  ],
                  zIndex: 1000,
                }
              : { ...animations[it.id].getLayout(), zIndex: 1 },
          ]}
        >
          <TouchableWithoutFeedback
            delayLongPress={200}
            onLongPress={() => {
              setPanCapture(true);
              setTempSelected(it.id);
            }}
            onPress={() => (onTap ? onTap(it) : null)}
          >
            <View>
              <Item
                item={it}
                index={index}
                selected={it.id === selectedItemId}
              />
            </View>
          </TouchableWithoutFeedback>
        </Animated.View>
      ))}
    </View>
  );
};

export default DragDrop;
