import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableWithoutFeedback } from "react-native";
import DragDrop from "./DragDropView/DragDropView";

export default function App() {
  const [array, setArray] = useState([
    { id: "1" },
    { id: "2" },
    { id: "3" },
    { id: "4" },
  ]);
  const [additionalWidth, setAdditionalWidth] = useState(0);
  const [additionalHeight, setAdditionalHeight] = useState(0);
  const [numPerRow, setNumPerRow] = useState(3);
  const [gap, setGap] = useState(8);

  const onAddItem = () =>
    setArray([...array, { id: (+array[array.length - 1].id + 1).toString() }]);
  const onAddItemWidth = () => setAdditionalWidth(20);
  const onAddItemHeight = () => setAdditionalHeight(20);
  const changeNumberPerRow = () => setNumPerRow(4);
  const changeGap = () => setGap(16);

  return (
    <View style={styles.container}>
      <DragDrop
        data={array}
        itemHeight={60 + additionalHeight}
        itemWidth={60 + additionalWidth}
        gap={gap}
        numPerRow={numPerRow}
        onTap={(item) => console.log(item)}
        onDragEnd={(newArray) => setArray(newArray)}
        Item={({ item }) => (
          <View
            style={{
              height: 60 + additionalHeight,
              width: 60 + additionalWidth,
              backgroundColor:
                item.id === "1"
                  ? "green"
                  : item.id === "2"
                  ? "blue"
                  : item.id === "3"
                  ? "purple"
                  : item.id === "4"
                  ? "orange"
                  : "yellow",
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 6,
            }}
          >
            <Text>{item.id}</Text>
          </View>
        )}
      />
      <TouchableWithoutFeedback onPress={onAddItem}>
        <View
          style={{
            width: 200,
            paddingVertical: 8,
            backgroundColor: "#d8d8d8",
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text>Add Item</Text>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={onAddItemWidth}>
        <View
          style={{
            width: 200,
            paddingVertical: 8,
            backgroundColor: "#d8d8d8",
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text>Add Item Width</Text>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={onAddItemHeight}>
        <View
          style={{
            width: 200,
            paddingVertical: 8,
            backgroundColor: "#d8d8d8",
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text>Add Item Height</Text>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={changeNumberPerRow}>
        <View
          style={{
            width: 200,
            paddingVertical: 8,
            backgroundColor: "#d8d8d8",
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text>Change Number Per Row</Text>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={changeGap}>
        <View
          style={{
            width: 200,
            paddingVertical: 8,
            backgroundColor: "#d8d8d8",
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text>Change Gap</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 40,
    alignItems: "center",
  },
});
